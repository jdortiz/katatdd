//
//  Blend.h
//  TeaWishlist
//
//  Created by Jorge D. Ortiz Fuentes on 01/07/13.
//  Copyright (c) 2013 NSCoderMAD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Category;

@interface Blend : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSDecimalNumber * quantity;
@property (nonatomic, retain) NSDecimalNumber * pricePerKg;
@property (nonatomic, retain) Category *category;

@end
