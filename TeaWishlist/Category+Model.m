//
//  Category+Model.m
//  TeaWishlist
//
//  Created by Jorge D. Ortiz Fuentes on 29/06/13.
//  Copyright (c) 2013 NSCoderMAD. All rights reserved.
//


#import "Category+Model.h"


@implementation Category (Model)

+ (Category *) categoryInMOC:(NSManagedObjectContext *)moc withName:(NSString *)name {
    Category *category;
    if (moc != nil) {
        category = [NSEntityDescription insertNewObjectForEntityForName:@"Category"
                                                 inManagedObjectContext:moc];
        category.name = name;
    }
    return category;
}


#pragma mark - Query

+ (Category *) fetchCategoryInMOC:(NSManagedObjectContext *)moc withName:(NSString *)name {
    Category *category;
    if (moc != nil) {
        NSFetchRequest *fetchRequest = [Category prepareFetchCategoryInMoc:moc withName:name];
        NSError *error;
        NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
        if ([results count] != 0) {
            NSAssert(([results count] == 1), @"More than one category with the same name and group exist.");
            category = [results objectAtIndex:0];
        }
    }

    return category;
}


+ (NSFetchRequest *)prepareFetchCategoryInMoc:(NSManagedObjectContext *)moc withName:(NSString *)name {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = [NSEntityDescription entityForName:@"Category"
                                      inManagedObjectContext:moc];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(name == %@)",
                              name];
    return fetchRequest;
}


#pragma mark - Duplication

- (BOOL) isDuplicated {
    BOOL duplicated = NO;
    NSFetchRequest *fetchRequest = [Category prepareFetchCategoryInMoc:self.managedObjectContext
                                                              withName:self.name];
    NSError *error;
    NSUInteger count = [self.managedObjectContext countForFetchRequest:fetchRequest
                                                                 error:&error];
    if (count > 1) {
        duplicated = YES;
    }

    return duplicated;
}

@end
