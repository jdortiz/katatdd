//
//  Category.m
//  TeaWishlist
//
//  Created by Jorge D. Ortiz Fuentes on 01/07/13.
//  Copyright (c) 2013 NSCoderMAD. All rights reserved.
//

#import "Category.h"
#import "Blend.h"


@implementation Category

@dynamic name;
@dynamic notes;
@dynamic blends;

@end
