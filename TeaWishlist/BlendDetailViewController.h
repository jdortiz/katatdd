//
//  BlendDetailViewController.h
//  TeaWishlist
//
//  Created by Jorge D. Ortiz Fuentes on 04/07/13.
//  Copyright (c) 2013 NSCoderMAD. All rights reserved.
//


#import <UIKit/UIKit.h>

@class Blend;


@interface BlendDetailViewController : UIViewController

@property (strong, nonatomic) Blend *blend;

@property (weak, nonatomic) IBOutlet UITextField *blendNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *categoryNameTextField;

@end
