//
//  BlendsViewController.m
//  TeaWishlist
//
//  Created by Jorge D. Ortiz Fuentes on 03/07/13.
//  Copyright (c) 2013 NSCoderMAD. All rights reserved.
//


#import "BlendsViewController.h"
#import "BlendDetailViewController.h"
#import "Blend+Model.h"


@interface BlendsViewController ()

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@end


@implementation BlendsViewController

#pragma mark - Constants & Parameters

static NSString *const blendEntityName = @"Blend";
// Segues.
static NSString *const segueNewBlend = @"NewBlend";
static NSString *const segueShowBlend = @"ShowBlend";


#pragma mark - View life cycle

- (void) viewDidLoad {
    [super viewDidLoad];

    [self verifyOutlets];
    [self loadData];
}


- (void) verifyOutlets {
    NSAssert(self.managedObjectContext != nil,
             @"A managed object context is required for this view controller to work.");
}


- (void) loadData {
    // Fetch the elements for this table.
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Update to handle the error appropriately.
        NSLog(@"ERR: [%s:%s:%d] Unresolved error %@, %@", __FILE__, __FUNCTION__,
              __LINE__, error, [error userInfo]);
        exit(-1);  // Fail
    }
}


#pragma mark - UI actions

- (IBAction) addBlend:(id)sender {
    [self performSegueWithIdentifier:segueNewBlend sender:self];
}


#pragma mark - Segues

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:segueNewBlend]) {
        Blend *blend = [Blend blendInMOC:self.managedObjectContext withName:nil inCategory:nil];
        [self initializeBlendDetailViewController:segue.destinationViewController withBlend:blend];
    } else if ([segue.identifier isEqualToString:segueShowBlend]) {
        Blend *blend = [self getSelectedBlend];
        [self initializeBlendDetailViewController:segue.destinationViewController withBlend:blend];
    }
}


- (void) initializeBlendDetailViewController:(BlendDetailViewController *)blendDetailViewController
                                   withBlend:(Blend *)blend {
    blendDetailViewController.blend = blend;
}


- (Blend *) getSelectedBlend {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    return [self.fetchedResultsController objectAtIndexPath:indexPath];
}


#pragma mark - Table view data source

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger sections = [[self.fetchedResultsController sections] count];
    return sections;
}


- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger numberOfRows = 0;

    if ([[self.fetchedResultsController sections] count] > 0) {
        id<NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections]
                                                       objectAtIndex:section];
        numberOfRows = [sectionInfo numberOfObjects];
    }

    return numberOfRows;
}


- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"BlendCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}


- (void) configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Blend *blend = [self blendAtIndexPath:indexPath];
    [self displayNameOfBlend:blend inCell:cell];
}


- (void) displayNameOfBlend:(Blend *)blend inCell:(UITableViewCell *)cell {
    cell.textLabel.text = blend.name;
}


- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [[[self.fetchedResultsController sections] objectAtIndex:section] name];
}


#pragma mark - Fetched results controller

- (NSFetchedResultsController *) fetchedResultsController {
    static NSString *const fetchedSectionsKeyPath = @"category.name";
    if (_fetchedResultsController == nil) {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        fetchRequest.entity = [NSEntityDescription entityForName:blendEntityName
                                          inManagedObjectContext:self.managedObjectContext];

        NSSortDescriptor *categoryDescriptor = [[NSSortDescriptor alloc] initWithKey:fetchedSectionsKeyPath

                                                                           ascending:YES];
        NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                                       ascending:YES];
        [fetchRequest setSortDescriptors:@[categoryDescriptor, nameDescriptor]];

        _fetchedResultsController = [[NSFetchedResultsController alloc]
                                     initWithFetchRequest:fetchRequest
                                     managedObjectContext:self.managedObjectContext
                                     sectionNameKeyPath:fetchedSectionsKeyPath
                                     cacheName:@"Blends"];
    }

    return _fetchedResultsController;
}


- (Blend *) blendAtIndexPath:(NSIndexPath *)indexPath {
    return  [self.fetchedResultsController objectAtIndexPath:indexPath];
}

@end
