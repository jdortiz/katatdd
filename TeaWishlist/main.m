//
//  main.m
//  TeaWishlist
//
//  Created by Jorge D. Ortiz Fuentes on 27/06/13.
//  Copyright (c) 2013 NSCoderMAD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
