//
//  BlendsViewController.h
//  TeaWishlist
//
//  Created by Jorge D. Ortiz Fuentes on 03/07/13.
//  Copyright (c) 2013 NSCoderMAD. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface BlendsViewController : UITableViewController

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (IBAction) addBlend:(id)sender;

@end
