//
//  Blend+Model.h
//  TeaWishlist
//
//  Created by Jorge D. Ortiz Fuentes on 01/07/13.
//  Copyright (c) 2013 NSCoderMAD. All rights reserved.
//


#import "Blend.h"

@class Category;

typedef enum BlendStatus : NSUInteger {
    BlendStatusUntested = 0,
    BlendStatusDontLike,
    BlendStatusLike
} BlendStatus;


@interface Blend (Model)

+ (Blend *) blendInMOC:(NSManagedObjectContext *)moc withName:(NSString *)name inCategory:(Category *)category;
+ (Blend *) fetchBlendInMOC:(NSManagedObjectContext *)context withName:(NSString *)name inCategory:(Category *)category;
- (BOOL) isDuplicated;

@end
