//
//  BlendDetailViewController.m
//  TeaWishlist
//
//  Created by Jorge D. Ortiz Fuentes on 04/07/13.
//  Copyright (c) 2013 NSCoderMAD. All rights reserved.
//


#import "BlendDetailViewController.h"
#import "Category+Model.h"
#import "Blend+Model.h"


@interface BlendDetailViewController ()

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end


@implementation BlendDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self verifyOutlets];
    [self displayData];
}


- (void) verifyOutlets {
    NSAssert(self.blend != nil, @"A blend must be set to be edited or shown.");
}


- (void) displayData {
    [self displayBlendName];
    [self displayCategoryName];
}


- (void) displayBlendName {
    self.blendNameTextField.text = self.blend.name;
}


- (void) displayCategoryName {
    self.categoryNameTextField.text = self.blend.category.name;
}


- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.blend.name = self.blendNameTextField.text;
    self.blend.category = [self existingOrNewCategoryWithName:self.categoryNameTextField.text];
    [self safeChangesInMOC];
}


- (Category *) existingOrNewCategoryWithName:(NSString *)categoryName {
    Category *category = [Category fetchCategoryInMOC:self.managedObjectContext withName:categoryName];
    if (category == nil) {
        category = [Category categoryInMOC:self.managedObjectContext withName:categoryName];
    }
    return category;
}


- (void)safeChangesInMOC {
    if ([self.managedObjectContext hasChanges]) {
        NSError *error;
        [self.managedObjectContext save:&error];
    }
}


#pragma mark - Dependency injection

- (NSManagedObjectContext *) managedObjectContext {
    if (_managedObjectContext == nil) {
        _managedObjectContext = self.blend.managedObjectContext;
    }

    return _managedObjectContext;
}

@end
