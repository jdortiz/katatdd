//
//  Blend.m
//  TeaWishlist
//
//  Created by Jorge D. Ortiz Fuentes on 01/07/13.
//  Copyright (c) 2013 NSCoderMAD. All rights reserved.
//

#import "Blend.h"
#import "Category.h"


@implementation Blend

@dynamic name;
@dynamic status;
@dynamic quantity;
@dynamic pricePerKg;
@dynamic category;

@end
