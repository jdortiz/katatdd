//
//  Blend+Model.m
//  TeaWishlist
//
//  Created by Jorge D. Ortiz Fuentes on 01/07/13.
//  Copyright (c) 2013 NSCoderMAD. All rights reserved.
//


#import "Blend+Model.h"
#import "Category+Model.h"


@implementation Blend (Model)

#pragma mark - Parameters & Constants

static NSString *const blendEntityName = @"Blend";
static const float minBlendQuantity = 0.1;


#pragma mark - Convenience constructor

+ (Blend *) blendInMOC:(NSManagedObjectContext *)moc withName:(NSString *)name inCategory:(Category *)category {
    Blend *blend;
    if (moc != nil) {
        blend = [NSEntityDescription insertNewObjectForEntityForName:blendEntityName
                                              inManagedObjectContext:moc];
        blend.name = name;
        blend.category = category;
    }
    return blend;
}

#pragma mark - Query

+ (Blend *) fetchBlendInMOC:(NSManagedObjectContext *)context withName:(NSString *)name inCategory:(Category *)category {
    Blend *blend;
    NSFetchRequest *fetchRequest = [Blend prepareFetchBlendInMOC:context withName:name inCategory:category];
    NSError *error;
    NSArray *results = [context executeFetchRequest:fetchRequest error:&error];
    if ([results count] != 0) {
        NSAssert(([results count] == 1), @"More than one tea with the same name and group exist.");
        blend = [results objectAtIndex:0];
    }
    return blend;
}


+ (NSFetchRequest *) prepareFetchBlendInMOC:(NSManagedObjectContext *)moc
                                   withName:(NSString *)name
                                 inCategory:(Category *)category {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = [NSEntityDescription entityForName:blendEntityName
                                      inManagedObjectContext:moc];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(name == %@) AND (category == %@)", name, category];
    return fetchRequest;
}


#pragma mark - Duplication

- (BOOL) isDuplicated {
    BOOL duplicated = NO;
    NSFetchRequest *fetchRequest = [Blend prepareFetchBlendInMOC:self.managedObjectContext
                                                        withName:self.name
                                                      inCategory:self.category];
    NSError *error;
    NSUInteger count = [self.managedObjectContext countForFetchRequest:fetchRequest
                                                                 error:&error];
    if (count > 1) {
        duplicated = YES;
    }

    return duplicated;
}


#pragma mark -  Model logic

/**
 If the status is set to like the quantity must be, at least, the minimum one.
 If it is set to untested or don't like, then the quantity must be zero.
 */
- (void) setStatus:(NSNumber *)status {
    float currentQuantity;
    [self willChangeValueForKey:@"status"];
    [self setPrimitiveValue:status forKey:@"status"];
    [self didChangeValueForKey:@"status"];
    currentQuantity = [[self primitiveValueForKey:@"quantity"] floatValue];
    if ([status unsignedIntegerValue] == BlendStatusLike && currentQuantity < minBlendQuantity) {
        [self willChangeValueForKey:@"quantity"];
        [self setPrimitiveValue:[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f",
                                                                          minBlendQuantity]]
                         forKey:@"quantity"];
        [self didChangeValueForKey:@"quantity"];
    } else if (([status integerValue] == BlendStatusUntested || [status integerValue] == BlendStatusDontLike)
               && currentQuantity > 0.0) {
        [self willChangeValueForKey:@"quantity"];
        [self setPrimitiveValue:[NSDecimalNumber zero]
                         forKey:@"quantity"];
        [self didChangeValueForKey:@"quantity"];
    }
}


/**
 If quantity is set to something bigger than zero, the status must be Like.
 If it set to zero, and the status was like, it is set to dont like (untested makes no sense).
 */
- (void) setQuantity:(NSDecimalNumber *)quantity {
    BlendStatus currentStatus = [[self primitiveValueForKey:@"status"] unsignedIntegerValue];
    [self willChangeValueForKey:@"quantity"];
    [self setPrimitiveValue:quantity forKey:@"quantity"];
    [self didChangeValueForKey:@"quantity"];
    if ([quantity floatValue] > 0.0 && currentStatus != BlendStatusLike) {
        [self willChangeValueForKey:@"status"];
        [self setPrimitiveValue:@(BlendStatusLike) forKey:@"status"];
        [self didChangeValueForKey:@"status"];
    } else if ([quantity floatValue] < 0.01  && currentStatus == BlendStatusLike) {
        [self willChangeValueForKey:@"status"];
        [self setPrimitiveValue:@(BlendStatusDontLike) forKey:@"status"];
        [self didChangeValueForKey:@"status"];
    }
}

@end
