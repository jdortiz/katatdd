//
//  Category+Model.h
//  TeaWishlist
//
//  Created by Jorge D. Ortiz Fuentes on 29/06/13.
//  Copyright (c) 2013 NSCoderMAD. All rights reserved.
//


#import "Category.h"


@interface Category (Model)

+ (Category *) categoryInMOC:(NSManagedObjectContext *)moc withName:(NSString *)name;
+ (Category *) fetchCategoryInMOC:(NSManagedObjectContext *)moc withName:(NSString *)name;
- (BOOL) isDuplicated;

@end
