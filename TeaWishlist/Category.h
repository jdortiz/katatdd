//
//  Category.h
//  TeaWishlist
//
//  Created by Jorge D. Ortiz Fuentes on 01/07/13.
//  Copyright (c) 2013 NSCoderMAD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Blend;

@interface Category : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic, retain) NSSet *blends;
@end

@interface Category (CoreDataGeneratedAccessors)

- (void)addBlendsObject:(Blend *)value;
- (void)removeBlendsObject:(Blend *)value;
- (void)addBlends:(NSSet *)values;
- (void)removeBlends:(NSSet *)values;

@end
