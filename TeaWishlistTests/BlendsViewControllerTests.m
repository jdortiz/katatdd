//
//  BlendsViewControllerTests.m
//  TeaWishlist
//
//  Created by Jorge D. Ortiz Fuentes on 03/07/13.
//  Copyright (c) 2013 NSCoderMAD. All rights reserved.
//


#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "BlendsViewController.h"
#import "BlendDetailViewController.h"
#import "Category+Model.h"
#import "Blend+Model.h"


@interface BlendsViewControllerTests : XCTestCase {
    // Core Data stack objects.
    NSManagedObjectModel *model;
    NSPersistentStoreCoordinator *coordinator;
    NSPersistentStore *store;
    NSManagedObjectContext *context;
    // Object to test.
    BlendsViewController *sut;

    // Data to verify table view data source.
    Category *category1, *category2;
    Blend *blend1, *blend2, *blend3;
}

@end


@implementation BlendsViewControllerTests

#pragma mark - Parameters & Constants

static NSString *const storyboardName = @"TeaWishlist_iPhone";
static NSString *const blendsViewControllerID = @"BlendsViewController";
// Segues.
static NSString *const segueNewBlend = @"NewBlend";
static NSString *const segueShowBlend = @"ShowBlend";
// Data.
static NSInteger categorySectionsCount = 2;
static NSInteger blendsInCategory1 = 2;
static NSInteger blendsInCategory2 = 1;
static NSString *const categoryNameFirst  = @"A Category";
static NSString *const categoryNameSecond = @"B Category";
static NSString *const categoryNameThird  = @"C Category";
static NSString *const blendNameFirst  = @"A Blend";
static NSString *const blendNameSecond = @"B Blend";
static NSString *const blendNameThird  = @"C Blend";


#pragma mark - Set up and tear down

- (void) setUp {
    [super setUp];

    // Create the Core Data stack.
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    model = [NSManagedObjectModel mergedModelFromBundles:@[bundle]];
    coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
    store = [coordinator addPersistentStoreWithType: NSInMemoryStoreType
                                      configuration: nil
                                                URL: nil
                                            options: nil
                                              error: NULL];
    context = [[NSManagedObjectContext alloc] init];
    [context setPersistentStoreCoordinator:coordinator];

    [self createData];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    sut = [storyboard instantiateViewControllerWithIdentifier:blendsViewControllerID];
    sut.managedObjectContext = context;
}


- (void) createData {
    category1 = [Category categoryInMOC:context withName:categoryNameFirst];
    category2 = [Category categoryInMOC:context withName:categoryNameSecond];

    blend1 = [Blend blendInMOC:context withName:blendNameFirst inCategory:category1];
    blend2 = [Blend blendInMOC:context withName:blendNameSecond inCategory:category2];
    blend3 = [Blend blendInMOC:context withName:blendNameThird inCategory:category1];
}


- (void) tearDown {
    sut = nil;

    [self releaseData];

    context = nil;
    store = nil;
    coordinator = nil;
    model = nil;

    [super tearDown];
}

- (void) releaseData {
    blend1 = nil;
    blend2 = nil;
    blend3 = nil;

    category1 = nil;
    category2 = nil;
}


#pragma mark - Basic test

- (void) testObjectIsNotNil {
    XCTAssertNotNil(sut, @"The object to test must be created in setUp.");
}


#pragma mark - View controller loading

- (void) testFetchedResultsControllerIsCreatedOnDemmand {
    XCTAssertNotNil([sut valueForKey:@"fetchedResultsController"],
                    @"A fetched results controller must be created on demmand.");
}


- (void) testOnViewDidLoadAssertsDontCauseExceptions {
    XCTAssertNoThrow([sut view], @"Outlets verification shouldn't throw any exception.");
}


- (void) testOnViewDidLoadDataIsLoaded {
    NSError *__autoreleasing *err = (NSError *__autoreleasing *) [OCMArg anyPointer];
    id fetchedResultsControllerMock = [OCMockObject mockForClass:[NSFetchedResultsController class]];
    [[[fetchedResultsControllerMock expect] andReturnValue:OCMOCK_VALUE((BOOL){YES})] performFetch:err];
    [sut setValue:fetchedResultsControllerMock forKey:@"fetchedResultsController"];

    // Used to load the view.
    [sut view];

    XCTAssertNoThrow([fetchedResultsControllerMock verify],
                    @"On viewDidLoad data must be loaded in a fetched results controller.");
}


#pragma mark - UI actions


- (void) testAddChecklistSetsNewChecklistInChecklistToEdit {
    id sutMock = [OCMockObject partialMockForObject:sut];
    [[sutMock expect] performSegueWithIdentifier:segueNewBlend sender:sut];

    [sutMock addBlend:nil];

    XCTAssertNoThrow([sutMock verify],
                    @"addBlend actions should perform a segue.");
}



#pragma mark - Segues

- (void) testNewBlendSegueInitializesBlendDetailViewControllerWithNewBlend {
    id blendDetailViewControllerMock = [OCMockObject niceMockForClass:[BlendDetailViewController class]];
    [[blendDetailViewControllerMock expect] setBlend:OCMOCK_ANY];
    UIStoryboardSegue *storyboardSegue = [[UIStoryboardSegue alloc] initWithIdentifier:segueNewBlend
                                                                                source:sut
                                                                           destination:blendDetailViewControllerMock];

    [sut prepareForSegue:storyboardSegue sender:nil];

    XCTAssertNoThrow([blendDetailViewControllerMock verify],
                    @"Blend detail view controller must be initialized and a new blend passed to it.");
}


- (void) testShowBlendSegueInitializesBlendDetailViewControllerWithSelectedBlend {
    id tableViewMock = [OCMockObject partialMockForObject:sut.tableView];
    [[[tableViewMock stub] andReturn:[NSIndexPath indexPathForRow:0 inSection:1]] indexPathForSelectedRow];
    sut.tableView = tableViewMock;
    id blendDetailViewControllerMock = [OCMockObject niceMockForClass:[BlendDetailViewController class]];
    [[blendDetailViewControllerMock expect] setBlend:OCMOCK_ANY];
    UIStoryboardSegue *storyboardSegue = [[UIStoryboardSegue alloc] initWithIdentifier:segueShowBlend
                                                                                source:sut
                                                                           destination:blendDetailViewControllerMock];

    [sut prepareForSegue:storyboardSegue sender:nil];

    XCTAssertNoThrow([blendDetailViewControllerMock verify],
                    @"Blend detail view controller must be initialized and corresponding blend passed to it.");
}


#pragma mark - Table view

- (void) testTableViewNumberOfSectionsIsNumberOfCategories {
    [sut view];

    NSInteger sections = [sut numberOfSectionsInTableView:sut.tableView];

    XCTAssertEqual(sections, categorySectionsCount,
                   @"Number of sections must correspond to the number of categories defined.");
}


- (void) testTableViewNumberOfSectionsChangesWithNumberOfCategories {
    Category *category3 = [Category categoryInMOC:context withName:categoryNameThird];
    Blend *blend4 = [Blend blendInMOC:context withName:blendNameFirst inCategory:category3];

    [sut view];

    NSInteger sections = [sut numberOfSectionsInTableView:sut.tableView];

    XCTAssertEqual(sections, categorySectionsCount + 1,
                   @"Number of sections must correspond to the number of categories defined.");
    category3 = nil;
    blend4 = nil;
}


- (void) testTableViewNumberOfRowsOfFirstSectionIsBlendsInFirstCategory {
    [sut view];

    NSInteger rows = [sut tableView:sut.tableView numberOfRowsInSection:0];

    XCTAssertEqual(rows, blendsInCategory1,
                   @"Rows in section 0 must correspond to the number of blends in the first category.");
}


- (void) testTableViewNumberOfRowsOfSecondSectionIsBlendsInSecondCategory {
    [sut view];

    NSInteger rows = [sut tableView:sut.tableView numberOfRowsInSection:1];

    XCTAssertEqual(rows, blendsInCategory2,
                   @"Rows in section 1 must correspond to the number of blends in the second category.");
}


- (void) testTableViewNumberOfRowsOfFirstSectionOfEmptyContextIsZero {
    NSManagedObjectContext *emptyContext = [[NSManagedObjectContext alloc] init];
    emptyContext.persistentStoreCoordinator = coordinator;
    sut.managedObjectContext = emptyContext;
    [sut view];

    XCTAssertNoThrow([sut tableView:sut.tableView numberOfRowsInSection:0],
                    @"Obtaining the rows of a section of an empty context must not fail with exception.");
}


- (void) testTableViewReturnsNotNilCell {
    UITableViewCell *cell = [sut tableView:sut.tableView
                     cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];

    XCTAssertNotNil(cell, @"A valid cell must be returned by the table view.");
}


- (void) testCorrespondingBlendIsShownInCell {
    UITableViewCell *cell = [sut tableView:sut.tableView
                     cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];

    XCTAssertTrue([cell.textLabel.text isEqualToString:blendNameThird],
                 @"Text in cell should be %@, but is %@.", blendNameThird, cell.textLabel.text);

}


- (void) testSectionTitleForFirstSectionIsFirstCategoryName {
    NSString *title = [sut tableView:sut.tableView titleForHeaderInSection:0];

    XCTAssertTrue([title isEqualToString:categoryNameFirst],
                 @"First section title header is %@ but it should be %@.",
                 title, categoryNameFirst);
}


- (void) testSectionTitleForSecondSectionIsSecondCategoryName {
    NSString *title = [sut tableView:sut.tableView titleForHeaderInSection:1];

    XCTAssertTrue([title isEqualToString:categoryNameSecond],
                 @"First section title header is %@ but it should be %@.",
                 title, categoryNameSecond);
}

@end
