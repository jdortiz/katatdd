//
//  AppDelegateTests.m
//  TeaWishlist
//
//  Created by Jorge D. Ortiz Fuentes on 09/07/13.
//  Copyright (c) 2013 NSCoderMAD. All rights reserved.
//


#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "AppDelegate.h"
#import "BlendsViewController.h"


@interface AppDelegateTests : XCTestCase {
    // Core Data stack objects.
    NSManagedObjectModel *model;
    NSPersistentStoreCoordinator *coordinator;
    NSPersistentStore *store;
    NSManagedObjectContext *context;
    // Object to test.
    AppDelegate *sut;
}

@end


@implementation AppDelegateTests

#pragma mark - Set up and tear down

- (void) setUp {
    [super setUp];

//    [self createCoreDataStack];
    [self createSut];
}


- (void) createCoreDataStack {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    model = [NSManagedObjectModel mergedModelFromBundles:@[bundle]];
    coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
    store = [coordinator addPersistentStoreWithType: NSInMemoryStoreType
                                      configuration: nil
                                                URL: nil
                                            options: nil
                                              error: NULL];
    context = [[NSManagedObjectContext alloc] init];
    context.persistentStoreCoordinator = coordinator;
}


- (void) createSut {
    sut = [[AppDelegate alloc] init];
}


- (void) tearDown {
    [self releaseSut];
//    [self releaseCoreDataStack];

    [super tearDown];
}


- (void) releaseSut {
    sut = nil;
}


- (void) releaseCoreDataStack {
    context = nil;
    store = nil;
    coordinator = nil;
    model = nil;
}


#pragma mark - Basic test

- (void) testObjectIsNotNil {
    XCTAssertNotNil(sut, @"The object to test must be created in setUp.");
}


- (void) testApplicationDidFinishLaunchingReturnsYes {
    BOOL retVal = [sut application:nil didFinishLaunchingWithOptions:nil];
    XCTAssertTrue(retVal, @"Application delegate should return YES when invoked without a URL.");
}


- (void) testBlendsViewControllerIsConfiguredWithMOC {
    id blendsViewControllerMock = [OCMockObject niceMockForClass:[BlendsViewController class]];
    [[blendsViewControllerMock expect] setManagedObjectContext:sut.managedObjectContext];
    [sut setValue:blendsViewControllerMock forKey:@"blendsViewController"];
    [sut application:nil didFinishLaunchingWithOptions:nil];

    XCTAssertNoThrow([blendsViewControllerMock verify],
                    @"BlendsViewControler must be configured with an NSManagedObjectContext.");
}


@end
