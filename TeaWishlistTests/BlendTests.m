//
//  BlendTests.m
//  TeaWishlist
//
//  Created by Jorge D. Ortiz Fuentes on 01/07/13.
//  Copyright (c) 2013 NSCoderMAD. All rights reserved.
//


#import <XCTest/XCTest.h>
#import "Blend+Model.h"
#import "Category+Model.h"


@interface BlendTests : XCTestCase {
    // Core Data stack objects.
    NSManagedObjectModel *model;
    NSPersistentStoreCoordinator *coordinator;
    NSPersistentStore *store;
    NSManagedObjectContext *context;
    // Object to test.
    Blend *sut;
    // Auxiliary objects
    Category *category;
}

@end


@implementation BlendTests

#pragma mark - Parameters & constants

static NSString *const mainBlendName = @"BlendName";
static NSString *const mainCategoryName = @"CategoryName";
static NSString *const altBlendName = @"Another BlendName";
static NSString *const altCategoryName = @"Another CategoryName";
static const float minBlendQuantity = 0.1;


#pragma mark - Set up and tear down

- (void) setUp {
    [super setUp];

    // Create the Core Data stack.
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    model = [NSManagedObjectModel mergedModelFromBundles:@[bundle]];
    coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
    store = [coordinator addPersistentStoreWithType: NSInMemoryStoreType
                                      configuration: nil
                                                URL: nil
                                            options: nil
                                              error: NULL];
    context = [[NSManagedObjectContext alloc] init];
    [context setPersistentStoreCoordinator:coordinator];

    category = [Category categoryInMOC:context withName:mainCategoryName];
    sut = [Blend blendInMOC:context withName:mainBlendName inCategory:category];
}

- (void) tearDown {
    sut = nil;
    category = nil;
    context = nil;
    store = nil;
    coordinator = nil;
    model = nil;

    [super tearDown];
}


#pragma mark - Basic test

- (void) testObjectIsNotNil {
    XCTAssertNotNil(sut, @"The object to test must be created in setUp.");
}


#pragma mark - Convenience constructor

- (void) testConvenienceConstructorPreservesName {
    XCTAssertTrue([sut.name isEqualToString:mainBlendName],
                 @"Convenience constructor must preserve the provided name (%@), but it is %@.",
                 mainBlendName, sut.name);
}


- (void) testConvenienceConstructorPreservesAnotherName {
    Blend *newBlend = [Blend blendInMOC:context withName:altBlendName inCategory:category];
    XCTAssertTrue([newBlend.name isEqualToString:altBlendName],
                 @"Convenience constructor must preserve the provided name (%@), but it is %@.",
                 altBlendName, newBlend.name);
}


- (void) testConvenienceConstructorPreservesCategory {
    XCTAssertEqualObjects(sut.category, category,
                         @"Convenience constructor must preserve the provided category.");
}


- (void) testConvenienceConstructorWithoutMOCReturnsNil {
    Blend *newBlend = [Blend blendInMOC:nil withName:mainBlendName inCategory:category];

    XCTAssertNil(newBlend,
                @"Convenience constructor without a MOC must return nil.");
}


- (void) testConvenienceConstructorWithoutNameReturnsIncompleteBlend {
    Blend *newBlend = [Blend blendInMOC:context withName:nil inCategory:category];
    XCTAssertNotNil(newBlend,
                   @"A new blend can be created without a name.");
    XCTAssertNil(newBlend.name,
                @"A Blend created with the convenience constructor and no name must have a nil name.");
    XCTAssertEqualObjects(newBlend.category, category,
                         @"Convenience constructor must preserve the provided category even when blend has no name.");
}


- (void) testConvenienceConstructorWithoutCategoryReturnsIncompleteBlend {
    Blend *newBlend = [Blend blendInMOC:context withName:altBlendName inCategory:nil];
    XCTAssertNotNil(newBlend,
                   @"A new blend can be created without a category.");
    XCTAssertTrue([newBlend.name isEqualToString:altBlendName],
                 @"Convenience constructor must preserve the provided name (%@) even without a category, but it is %@.",
                 altBlendName, newBlend.name);
    XCTAssertNil(newBlend.category,
                @"A blend created with the convenience constructor and no category must have a nil category.");
}


#pragma mark - Query

- (void) testQueryReturnsTheBlend {
    Blend *selectedBlend = [Blend fetchBlendInMOC:context withName:mainBlendName inCategory:category];

    XCTAssertEqualObjects(sut, selectedBlend,
                         @"Query should return the right existing Blend.");
}


- (void) testQueryForNonExistingBlendReturnsNil {
    Blend *selectedBlend = [Blend fetchBlendInMOC:context withName:altBlendName inCategory:category];

    XCTAssertNil(selectedBlend, @"Query for non existing Blend should return nil.");
}


- (void) testQueryForBlendInWrongCategoryReturnsNil {
    Category *otherCategory = [Category categoryInMOC:context withName:altCategoryName];

    Blend *selectedBlend = [Blend fetchBlendInMOC:context withName:mainBlendName inCategory:otherCategory];

    XCTAssertNil(selectedBlend, @"Query for non existing Blend should return nil.");
}


#pragma mark - Duplication

- (void) testBlendIsNotDuplicatedWhenOnlyOneExists {
    XCTAssertFalse([sut isDuplicated],
                  @"Blend cannot be duplicated if only one exists.");
}


- (void) testBlendIsDuplicatedIfTwoExistWithSameNameAndCategory {
    Blend *newBlend = [Blend blendInMOC:context
                               withName:mainBlendName
                                inCategory:category];


    XCTAssertTrue([sut isDuplicated],
                 @"Blend must be duplicated if another one with the same name and category exists.");
    newBlend = nil;
}


- (void) testBlendIsNotDuplicatedIfTwoExistWithSameNameButDifferentCategory {
    Category *otherCategory = [Category categoryInMOC:context withName:altCategoryName];
    Blend *newBlend = [Blend blendInMOC:context
                               withName:mainBlendName
                             inCategory:otherCategory];

    XCTAssertFalse([sut isDuplicated],
                  @"Blend cannot be duplicated if another one with the same name but in different category exists.");
    newBlend = nil;
}


#pragma mark - Status - quantity relationship

- (void) testBlendDefaultStatusIsUntested  {
    XCTAssertEqual([sut.status unsignedIntegerValue], BlendStatusUntested,
                   @"Default blend status must be untested.");
}


- (void) testBlendDefaultQuanityIsZero  {
    XCTAssertEqualWithAccuracy((float)[sut.quantity floatValue], 0.0f, 0.01,
                               @"Default quantity must be 0.0.");
}


- (void) testBlendStatusUntestedSetsQuantityToZero {
    sut.quantity = [NSDecimalNumber decimalNumberWithString:@"1.3"];
    sut.status = @(BlendStatusUntested);

    XCTAssertEqualWithAccuracy((float)[sut.quantity floatValue], 0.0f, 0.01,
                               @"Quantity must be set to 0.0 when status is set to UNTESTED.");
}


- (void) testBlendStatusDontLikeSetsQuantityToZero {
    sut.quantity = [NSDecimalNumber decimalNumberWithString:@"1.3"];
    sut.status = @(BlendStatusDontLike);

    XCTAssertEqualWithAccuracy((float)[sut.quantity floatValue], 0.0f, 0.01,
                               @"Quantity must be set to 0.0 when status is set to DONTLIKE.");
}


- (void) testBlendStatusLikeSetsQuantityToDefaultMin {
    sut.status = @(BlendStatusLike);

    XCTAssertEqualWithAccuracy([sut.quantity floatValue], minBlendQuantity, 0.01,
                               @"Quantity must be set to 0.1 when status is set to LIKE.");
}


- (void) testBlendQuantityBiggerThanZeroSetsStatusToLike {
    sut.quantity = [NSDecimalNumber one];

    XCTAssertEqual([sut.status unsignedIntegerValue], BlendStatusLike,
                   @"If the quantity is non-zero, status must be LIKE.");
}


- (void) testBlendQuantityZeroAndLikeStatusSetsStatusToDontLike {
    sut.status = @(BlendStatusLike);
    sut.quantity = [NSDecimalNumber zero];

    XCTAssertEqual([sut.status unsignedIntegerValue], BlendStatusDontLike,
                   @"If the quantity is non-zero, status must be LIKE.");
}

@end
