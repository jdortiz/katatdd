//
//  BlendDetailViewControllerTests.m
//  TeaWishlist
//
//  Created by Jorge D. Ortiz Fuentes on 04/07/13.
//  Copyright (c) 2013 NSCoderMAD. All rights reserved.
//


#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "BlendDetailViewController.h"
#import "Category+Model.h"
#import "Blend+Model.h"


@interface BlendDetailViewControllerTests : XCTestCase {
    // Core Data stack objects.
    NSManagedObjectModel *model;
    NSPersistentStoreCoordinator *coordinator;
    NSPersistentStore *store;
    NSManagedObjectContext *context;
    // Object to test.
    BlendDetailViewController *sut;
    // Auxiliary objects.
    Category *category;
    Blend *blend;
}

@end


@implementation BlendDetailViewControllerTests

#pragma mark - Parameters & Constants

static NSString *const storyboardName = @"TeaWishlist_iPhone";
static NSString *const blendDetailViewControllerID = @"BlendDetailViewController";
static NSString *const mainCategoryName = @"Category name";
static NSString *const mainBlendName = @"Blend name";
static NSString *const altCategoryName = @"Another Category name";
static NSString *const altBlendName = @"Another Blend name";


#pragma mark - Set up and tear down

- (void) setUp {
    [super setUp];

    [self createCoreDataStack];
    [self createData];
    [self createSut];
}


- (void) createCoreDataStack {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    model = [NSManagedObjectModel mergedModelFromBundles:@[bundle]];
    coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
    store = [coordinator addPersistentStoreWithType: NSInMemoryStoreType
                                      configuration: nil
                                                URL: nil
                                            options: nil
                                              error: NULL];
    context = [[NSManagedObjectContext alloc] init];
    context.persistentStoreCoordinator = coordinator;
}


- (void) createData {
    category = [Category categoryInMOC:context withName:mainCategoryName];
    blend = [Blend blendInMOC:context withName:mainBlendName inCategory:category];
}


- (void) createSut {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    sut = [storyboard instantiateViewControllerWithIdentifier:blendDetailViewControllerID];
    sut.blend = blend;
}


- (void) tearDown {
    [self releaseSut];
    [self releaseData];
    [self releaseCoreDataStack];

    [super tearDown];
}


- (void) releaseSut {
    sut = nil;
}


- (void) releaseData {
    blend = nil;
    category = nil;
}


- (void) releaseCoreDataStack {
    context = nil;
    store = nil;
    coordinator = nil;
    model = nil;
}


#pragma mark - Basic test

- (void) testObjectIsNotNil {
    XCTAssertNotNil(sut, @"The object to test must be created in setUp.");
}


#pragma mark - Outlets & actions connected

- (void) testBlendNameTextFieldIsNotNil {
    [sut view];

    XCTAssertNotNil(sut.blendNameTextField, @"blendNameTextField outlet must be connected in storyboard.");
}


- (void) testCategoryNameTextFieldIsNotNil {
    [sut view];

    XCTAssertNotNil(sut.categoryNameTextField, @"categoryNameTextField outlet must be connected in storyboard.");
}


#pragma mark - View controller loading

- (void) testOnViewDidLoadAssertsDontCauseExceptions {
    XCTAssertNoThrow([sut view], @"Outlets verification shouldn't throw any exception.");
}


- (void) testOnViewDidLoadBlendNameIsDisplayedInBlendNameTextField {
    [sut view];

    XCTAssertTrue([sut.blendNameTextField.text isEqualToString:mainBlendName],
                 @"Blend name text field should show the name of the blend.");
}


- (void) testOnViewDidLoadCategoryNameIsDisplayedInCategoryNameTextField {
    [sut view];

    XCTAssertTrue([sut.categoryNameTextField.text isEqualToString:mainCategoryName],
                 @"Category name text field should show the name of the blend.");
}


- (void) testViewWillDisappearUsesBlendName {
    [sut view];
    sut.blendNameTextField.text = altBlendName;

    [sut viewDidDisappear:NO];

    XCTAssertTrue([sut.blend.name isEqualToString:altBlendName],
                 @"Blend name must be the one entered in the text field.");
}


- (void) testViewWillDisappearCreatesNewCategoryIfRequired {
    [sut view];
    sut.categoryNameTextField.text = altCategoryName;

    [sut viewDidDisappear:NO];

    XCTAssertTrue([sut.blend.category.name isEqualToString:altCategoryName],
                 @"Blend category must be one with the configured name.");
}


- (void) testViewWillDisappearSavesChanges {
    NSError *__autoreleasing *err = (NSError *__autoreleasing *) [OCMArg anyPointer];
    id mocMock = [OCMockObject partialMockForObject:context];
    [[[mocMock stub] andReturnValue:OCMOCK_VALUE((BOOL){YES})] hasChanges];
    [[[mocMock expect] andReturnValue:OCMOCK_VALUE((BOOL){YES})] save:err];
    [sut setValue:mocMock forKey:@"managedObjectContext"];
    [sut view];

    [sut viewDidDisappear:NO];

    XCTAssertNoThrow([mocMock verify], @"Changes must be saved on viewWillDisappear.");
}

@end
