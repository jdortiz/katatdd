//
//  CategoryTests.m
//  TeaWishlist
//
//  Created by Jorge D. Ortiz Fuentes on 29/06/13.
//  Copyright (c) 2013 NSCoderMAD. All rights reserved.
//


#import <XCTest/XCTest.h>
#import "Category+Model.h"


@interface CategoryTests : XCTestCase {
    // Core Data stack objects.
    NSManagedObjectModel *model;
    NSPersistentStoreCoordinator *coordinator;
    NSPersistentStore *store;
    NSManagedObjectContext *context;
    // Object to test.
    Category *sut;
}

@end


@implementation CategoryTests

#pragma mark - Constants & Parameters

static NSString *const mainCategoryName = @"CategoryName";
static NSString *const altCategoryName = @"Another Category Name";


#pragma mark - Set up and tear down

- (void) setUp {
    [super setUp];

    // Create the Core Data stack.
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    model = [NSManagedObjectModel mergedModelFromBundles:@[bundle]];
    coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
    store = [coordinator addPersistentStoreWithType: NSInMemoryStoreType
                                      configuration: nil
                                                URL: nil
                                            options: nil
                                              error: NULL];
    context = [[NSManagedObjectContext alloc] init];
    [context setPersistentStoreCoordinator:coordinator];

    sut = [Category categoryInMOC:context withName:mainCategoryName];
}

- (void) tearDown {
    sut = nil;
    context = nil;
    store = nil;
    coordinator = nil;
    model = nil;

    [super tearDown];
}


#pragma mark - Basic test

- (void) testObjectIsNotNil {
    XCTAssertNotNil(sut, @"The category to test must be created in setUp.");
}


#pragma mark - Convenience constructor

- (void) testConvenienceConstructorPreservesName {
    XCTAssertTrue([sut.name isEqualToString:mainCategoryName],
                 @"Convenience constructor must preserve the provided name (%@), but it is %@.",
                 mainCategoryName, sut.name);
}


- (void) testConvenienceConstructorPreservesAltName {
    Category *newCategory = [Category categoryInMOC:context withName:altCategoryName];
    XCTAssertTrue([newCategory.name isEqualToString:altCategoryName],
                 @"Convenience constructor must preserve the provided name (%@), but it is %@.",
                 altCategoryName, newCategory.name);
}


- (void) testConvenienceConstructorWithoutMOCReturnsNil {
    Category *newCategory = [Category categoryInMOC:nil withName:mainCategoryName];

    XCTAssertNil(newCategory,
                @"Convenience constructor without a MOC must return nil.");
}


- (void) testConvenienceConstructorWithoutNameReturnsIncompleteCategory {
    Category *newCategory = [Category categoryInMOC:context withName:nil];
    XCTAssertNotNil(newCategory,
                   @"A new category can be created without a name.");
    XCTAssertNil(newCategory.name,
                @"A category created with the convenience constructor and no name must have a nil name.");
}


#pragma mark - Query

- (void) testQueryReturnsTheCategory {
    Category *selectedCategory = [Category fetchCategoryInMOC:context
                                                     withName:mainCategoryName];
    XCTAssertEqualObjects(sut, selectedCategory,
                         @"Query should return the right existing category.");
}


- (void) testQueryWithoutMOCReturnsNil {
    Category *selectedCategory = [Category fetchCategoryInMOC:nil
                                                     withName:mainCategoryName];
    XCTAssertNil(selectedCategory,
                @"Query should return nil without a MOC.");
}


- (void) testQueryWithNonexistingNameReturnsNil {
    Category *selectedCategory = [Category fetchCategoryInMOC:context
                                                     withName:altCategoryName];
    XCTAssertNil(selectedCategory,
                @"Query should return nil if category doesn't exist.");
}


#pragma mark - Duplication

- (void) testCategoryIsNotDuplicatedWhenOnlyOneExists {
    XCTAssertFalse([sut isDuplicated],
                  @"Category cannot be duplicated if only one exists.");
}


- (void) testCategoryIsDuplicatedIfTwoExistWithSameName {
    Category *newCategory = [Category categoryInMOC:context
                                           withName:mainCategoryName];

    XCTAssertTrue([sut isDuplicated],
                 @"Category must be duplicated if another one with the same name exists.");
    newCategory = nil;
}

@end
